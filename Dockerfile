FROM php:7.1.3-fpm
LABEL maintainers = "Mohamad Abdalbaqi Alsharbaji <msharbaji@gmail.com>"

RUN docker-php-ext-install pdo_mysql
RUN apt-get update && apt-get install -y \
        libpq-dev \
        libmcrypt-dev \
        curl \
    && docker-php-ext-install -j$(nproc) mcrypt \
    && docker-php-ext-install -j$(nproc) pdo \
    && docker-php-ext-install -j$(nproc) pdo_mysql \
    && docker-php-ext-install  mbstring

RUN apt-get install -y nginx  supervisor && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /var/www/html

COPY src/ /var/www/html
COPY ./entrypoint /var/www/html

RUN rm /etc/nginx/sites-enabled/default

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

ADD https://getcomposer.org/download/1.6.2/composer.phar /usr/bin/composer
RUN chmod +rx /usr/bin/composer

RUN composer install

RUN cp .env.example .env
RUN php artisan key:generate

RUN chmod +x ./entrypoint

RUN chmod -R 777 /var/www/html/storage
RUN chmod -R 777 /var/www/html/bootstrap
ENTRYPOINT ["./entrypoint"]

EXPOSE 80
