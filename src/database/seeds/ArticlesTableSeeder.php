<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        DB::table('articles')->insert([
            [
                'unique_id' => $faker->randomNumber(),
                'article_id' => $faker->randomNumber(),
                'published_date' => $faker->dateTime($max = 'now', $timezone = null),
                'modified_date' => $faker->dateTime($max = 'now', $timezone = null),
                'channel_id' => $faker->randomNumber(),
                'url' => $faker->url,
                'title' => $faker->word,
                'author' => $faker->name,
                'description' => $faker->paragraph,
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
            ],
            [
                'unique_id' => $faker->randomNumber(),
                'article_id' => $faker->randomNumber(),
                'published_date' => $faker->dateTime($max = 'now', $timezone = null),
                'modified_date' => $faker->dateTime($max = 'now', $timezone = null),
                'channel_id' => $faker->randomNumber(),
                'url' => $faker->url,
                'title' => $faker->word,
                'author' => $faker->name,
                'description' => $faker->paragraph,
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
            ],
            [
                'unique_id' => $faker->randomNumber(),
                'article_id' => $faker->randomNumber(),
                'published_date' => $faker->dateTime($max = 'now', $timezone = null),
                'modified_date' => $faker->dateTime($max = 'now', $timezone = null),
                'channel_id' => $faker->randomNumber(),
                'url' => $faker->url,
                'title' => $faker->word,
                'author' => $faker->name,
                'description' => $faker->paragraph,
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
            ],
            [
                'unique_id' => $faker->randomNumber(),
                'article_id' => $faker->randomNumber(),
                'published_date' => $faker->dateTime($max = 'now', $timezone = null),
                'modified_date' => $faker->dateTime($max = 'now', $timezone = null),
                'channel_id' => $faker->randomNumber(),
                'url' => $faker->url,
                'title' => $faker->word,
                'author' => $faker->name,
                'description' => $faker->paragraph,
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
            ],
            [
                'unique_id' => $faker->randomNumber(),
                'article_id' => $faker->randomNumber(),
                'published_date' => $faker->dateTime($max = 'now', $timezone = null),
                'modified_date' => $faker->dateTime($max = 'now', $timezone = null),
                'channel_id' => $faker->randomNumber(),
                'url' => $faker->url,
                'title' => $faker->word,
                'author' => $faker->name,
                'description' => $faker->paragraph,
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
            ],
            [
                'unique_id' => $faker->randomNumber(),
                'article_id' => $faker->randomNumber(),
                'published_date' => $faker->dateTime($max = 'now', $timezone = null),
                'modified_date' => $faker->dateTime($max = 'now', $timezone = null),
                'channel_id' => $faker->randomNumber(),
                'url' => $faker->url,
                'title' => $faker->word,
                'author' => $faker->name,
                'description' => $faker->paragraph,
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
            ]
        ]);
    }
}
