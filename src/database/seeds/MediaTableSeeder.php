<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        $articles = DB::table('articles')->pluck('id');
        DB::table('media')->insert([
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ],
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ],
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ],
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ],
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ],
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ],
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ],
            [
                'caption' => $faker->sentence,
                'url' => $faker->url,
                'article_id' => $faker->randomElement($articles),
                'created_at' => $faker->dateTime($max = 'now', $timezone = null),
                'updated_at' => $faker->dateTime($max = 'now', $timezone = null)
                
            ]
        ]);
    }
}
