<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('articles', 'ArticleController@index');
Route::get('articles/{article}', 'ArticleController@show');
Route::post('articles', 'ArticleController@store');
Route::put('articles/{article}', 'ArticleController@update');
Route::delete('articles/{article}', 'ArticleController@delete');
Route::get('article_media/{article}', 'ArticleController@article_media');

Route::get('media', 'MediaController@index');
Route::get('media/{media}', 'MediaController@show');
Route::post('media', 'MediaController@store');
Route::delete('media/{media}', 'MediaController@delete');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
