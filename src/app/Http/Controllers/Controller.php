<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @SWG\Swagger(
 *   basePath="/api",
 *   produces={"application/json"},
 *   consumes={"application/json"},
 *   host="echo-api.endpoints.YOUR-PROJECT-ID.cloud.goog",
 *   @SWG\Info(
 *     description="A Sample API for Travix HW",
 *     title="Article API",
 *     version="1.0.0",
 *     @SWG\Contact(name="msharbaj@gmail.com"),
 *   )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
