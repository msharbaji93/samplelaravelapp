<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $table = 'media';
    protected $fillable = ['caption', 'url', 'published_date', 'article_id'];

    public function article(){
        return $this->belongsTo("App\Article");
    }
}
