<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'articles';
    protected $fillable = ['unique_id', 'article_id', 'published_date', 'modified_date', 
    'channel_id', 'url', 'title', 'author', 'description'];

    public function media(){
        return $this->hasMany("App\Media");
    }
}
